# Utilities for applying stain normalization to images
import numpy as np

from sklearn.naive_bayes import GaussianNB


def get_normalization_params(cluster_means, cluster_eigs, template_params):
    """
    Compute the parameters for HSD-space normalization given cluster means and eigenvalues of the
    current image and the template_parameters (mean, eigs and density) of the target distribution

    :param cluster_means:
    :param cluster_eigs:
    :param template_params:
    :return: dictionary with 'shift', 'rot', 'scale' and 'density' transform params in the format
     expected by the normalize_stains_in_hsd function
    """

    normalization_params = {}

    template_means = template_params['means']
    template_eigs = template_params['eigs']
    template_density = template_params['density']

    t_shift = {}
    t_rot = {}
    t_scale = {}
    t_density = {}
    for key in ['H', 'E', 'B']:
        t_shift.update({key: template_means[key]})
        t_scale.update({key: template_eigs[key][1:] / cluster_eigs[key][1:3]})
        t_rot.update({key: [cluster_means[key][1], cluster_means[key][2],
                            -np.deg2rad(cluster_eigs[key][0]), -np.deg2rad(template_eigs[key][0])]})
        t_density.update(
            {key: [cluster_means[key][0], cluster_eigs[key][-1], template_density[key][0], template_density[key][1]]})

    normalization_params.update(
        {
            'shift': t_shift,
            'rot': t_rot,
            'scale': t_scale,
            'dens': t_density
        }
    )

    return normalization_params


def transform_to_hsd(patch, i0):
    """Given image and white-array values, get the HSD (Hue Saturation Density)
    channels
    """
    d = -np.log(patch / (i0 * np.ones_like(patch)))

    D = np.clip(np.mean(d, axis=2), 0.01, 2.0)
    cx = d[:, :, 2] / D - 1
    cy = (d[:, :, 1] - d[:, :, 0]) * 1 / (np.sqrt(3) * D)

    hsd_image = np.zeros_like(patch, dtype=np.float)
    hsd_image[:, :, 0] = D[:, :]
    hsd_image[:, :, 1] = cx[:, :]
    hsd_image[:, :, 2] = cy[:, :]

    return hsd_image, d[:, :, 2]


def hsd_to_rgb(hsd, i0):
    """Inverse to 'transform_to_hsd', takes an HSD image and the white channel intensity
    and transforms back to RGB color space"""

    bgr = np.zeros_like(hsd)

    Dr = (1 + hsd[:, :, 1]) * hsd[:, :, 0]
    Dg = 0.5 * hsd[:, :, 0] * (2 - hsd[:, :, 1] + np.sqrt(3) * hsd[:, :, 2])
    Db = 0.5 * hsd[:, :, 0] * (2 - hsd[:, :, 1] - np.sqrt(3) * hsd[:, :, 2])

    bgr[:, :, 2] = i0[2] * np.exp(-Dr)
    bgr[:, :, 1] = i0[1] * np.exp(-Dg)
    bgr[:, :, 0] = i0[0] * np.exp(-Db)

    p98 = np.percentile(bgr, q=98)
    scale_ch = 1.0
    if np.isfinite(p98) and p98 > 255.0:
        scale_ch = 255.0 / p98

    res_im = ((scale_ch*bgr).clip([0.0, 0.0, 0.0], [255.0, 255.0, 255.0]))
    white = 255 * np.ones_like(res_im)
    bg_mask = 1. / (1. + np.exp(-30 * (hsd[:, :, 0] - 0.15)))

    y = np.expand_dims(bg_mask, -1)
    res_im2 = ((1-y) * white) + (y*res_im)

    return res_im2.astype(np.uint8)


def normalize_stains_in_hsd(patch, i0_ch, transfer_params, training_d):
    """Normalize stains (in HSD space) to given

    :param patch: Input patch (RGB)
    :param i0_ch: Mean I_0 intensity (used in the optical density transform)
    :param transfer_params: Current to template transformation parameters (output of get_normalization_params)
    :param training_d: A dictionary containing training examples for 'H', 'E' and 'B' stains
    :return:
    """

    c_shift = transfer_params['shift']
    c_scale = transfer_params['scale']
    c_rot = transfer_params['rot']
    c_density = transfer_params['dens']

    hsd, Dr = transform_to_hsd(patch, i0_ch)

    X = np.concatenate([np.asarray(training_d['H'])[:, :3],
        np.asarray(training_d['E'])[:, :3],
        np.asarray(training_d['B'])[:, :3]])

    y = np.concatenate( [np.zeros(len(training_d['H']), dtype=np.uint8),
                         np.ones(len(training_d['E']), dtype=np.uint8),
                         2*np.ones(len(training_d['B']), dtype=np.uint8) ])

    gnb = GaussianNB()
    gnb.fit(X, y)

    x_pred = hsd.reshape(-1, 3)
    x_valid = np.zeros_like(x_pred)
    x_finite = np.isfinite(np.sum(x_pred, axis=1))
    x_valid[x_finite, :] = x_pred[x_finite, :]

    y_pred = gnb.predict_proba(x_valid).reshape((patch.shape[0], patch.shape[1], 3))
    hsd_t = np.zeros_like(hsd)
    hsd_t[:, :, 0] = hsd[:, :, 0]

    D = (hsd_t[:, :, 0]).reshape(-1, 1)

    Dh = ((D - c_density['H'][0]) / c_density['H'][1]) * c_density['H'][3] + c_density['H'][2]
    De = ((D - c_density['E'][0]) / c_density['E'][1]) * c_density['E'][3] + c_density['E'][2]
    Db = ((D - c_density['B'][0]) / c_density['B'][1]) * c_density['B'][3] + c_density['B'][2]

    Dh = Dh.reshape((patch.shape[0], patch.shape[1]))
    De = De.reshape((patch.shape[0], patch.shape[1]))
    Db = Db.reshape((patch.shape[0], patch.shape[1]))

    Rhb = np.array([
        [np.cos(c_rot['H'][2]), -np.sin(c_rot['H'][2])],
        [np.sin(c_rot['H'][2]), np.cos(c_rot['H'][2])]
    ])

    Rhf = np.array([
        [np.cos(c_rot['H'][3]), -np.sin(c_rot['H'][3])],
        [np.sin(c_rot['H'][3]), np.cos(c_rot['H'][3])]
    ])

    Reb = np.array([
        [np.cos(c_rot['E'][2]), -np.sin(c_rot['E'][2])],
        [np.sin(c_rot['E'][2]), np.cos(c_rot['E'][2])]
    ])

    Ref = np.array([
        [np.cos(c_rot['E'][3]), -np.sin(c_rot['E'][3])],
        [np.sin(c_rot['E'][3]), np.cos(c_rot['E'][3])]
    ])

    dh = (c_scale['H']*((hsd[:, :, 1:] - c_rot['H'][:2]).dot(Rhb))).dot(Rhf) + c_shift['H']
    de = (c_scale['E']*((hsd[:, :, 1:] - c_rot['E'][:2]).dot(Reb))).dot(Ref) + c_shift['E']
    db = hsd[:, :, 1:]

    y0 = np.expand_dims(y_pred[:, :, 0], -1)
    y1 = np.expand_dims(y_pred[:, :, 1], -1)
    y2 = np.expand_dims(y_pred[:, :, 2], -1)

    hsd_t[:, :, 1:] = y0 * dh[:, :] + y1 * de[:, :] + y2 * db[:, :]
    hsd_t[:, :, 0] = y_pred[:, :, 0] * Dh[:, :] + y_pred[:, :, 1] * De[:, :] + y_pred[:, :, 2] * Db[:, :]

    patch_normalized = hsd_to_rgb(hsd_t, i0_ch)

    return patch_normalized


def apply_normalization(input_image, stain_parameters, template_params):
    """
    Apply stain normalization to the given input image (ndarray) with stain parameters given as
    dictionary (stain_parameters) and the target (template) stain parameters in template_params

    :param input_image:
    :param stain_parameters:
    :param template_params: nested dictionary with top-level keys ('means', 'eigs', 'density')
    and for each the values are given for 'H', 'E', 'B'

    :return: normalized patch

    """

    return normalize_stains_in_hsd(input_image, stain_parameters['i0_ch'],
                                   get_normalization_params(stain_parameters['cluster_means'],
                                                            stain_parameters['cluster_eigs'],
                                                            template_params),
                                   stain_parameters['distributions'])
