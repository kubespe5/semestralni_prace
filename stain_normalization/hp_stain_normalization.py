"""Implementation of the
    'Stain specific standardization of whole-slide histopathological images'
    Bejnordi et al, TMI 2016

    Jan Hering <jan.hering@fel.cvut.cz>

"""
from __future__ import print_function
import os
import argparse
import matplotlib as mpl
#if os.environ.get('DISPLAY','') == '':
#    print('no display found. Using non-interactive Agg backend')
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

import cv2
import numpy as np
import pickle
from openslide import OpenSlide
from sklearn.neighbors import KNeighborsClassifier

import stain_normalization.utils as snu

from collections import OrderedDict

from microscopyio.slide_image import NDPISlideImage, CamelyonSlideImage
from feature_extraction.structural import blob_detection_watershed
from hp_utils.dict_to_csv import dictlist_to_csv

classes = ['H', 'E', 'B']
p_colors = ['r', 'b', 'g']


def sample_points(map, threshold, n_points):
    candidates = np.argwhere(map > threshold)
    if len(candidates) < 1:
        return []

    selection = np.random.choice(np.arange(len(candidates)), size=min(int(0.5 * len(candidates)), n_points))
    return candidates[selection]


def _get_distribution_parameters(distributions):

    Xh = np.asarray(distributions['H'])
    Xhc = Xh[:, :3]
    Xh_valid = np.where(np.min(Xhc - np.percentile(Xhc, 5, axis=0), axis=1) > 0) and np.where(np.max(Xhc - np.percentile(Xhc, 95, axis=0), axis=1) < 0)

    distributions['H'] = (Xh[Xh_valid]).tolist()

    Xe = np.asarray(distributions['E'])
    Xec = Xe[:, :3]
    Xe_valid = np.where(np.min(Xec - np.percentile(Xec, 5, axis=0), axis=1) > 0) and np.where(np.max(Xec - np.percentile(Xec, 95, axis=0), axis=1) < 0)
    distributions['E'] = (Xe[Xe_valid]).tolist()

    Xb = np.asarray(distributions['B'])
    Xbc = Xb[:, :3]
    Xb_valid = np.where(np.min(Xb - np.percentile(Xb, 10, axis=0), axis=1) > 0) and np.where(np.max(Xb - np.percentile(Xb, 90, axis=0), axis=1) < 0)
    distributions['B'] = (Xb[Xb_valid]).tolist()

    X = np.concatenate([Xhc[Xh_valid], Xec[Xe_valid], Xbc[Xb_valid]])
    y = np.concatenate( [np.zeros_like(Xh_valid[0], dtype=np.uint8),
                         np.ones_like(Xe_valid[0], dtype=np.uint8),
                         2*np.ones_like(Xb_valid[0], dtype=np.uint8)])

    clf = KNeighborsClassifier(n_neighbors=7)
    clf.fit(X, y)

    Xp = None
    if len(distributions['T']) > 0:

        Xt = np.asarray(distributions['T'])[:, :3]
        Xp = clf.predict(Xt)

    return clf, Xp


def _get_clusters(distributions, predictions, imname, output_dir, make_plot=True):

    means = {}
    eigs = {}

    if make_plot:
        fig = plt.figure()
        ax2 = fig.add_subplot('224')

    for cl_i, cl in enumerate(classes):
        training = np.asarray(distributions[cl])[:, 0:3]
        test = np.asarray(distributions['T'])[np.where(predictions == cl_i), 0:3]

        cl_data = np.concatenate([training, test.squeeze()])

        cx_mean = np.mean(cl_data, axis=0)
        means.update({cl: cx_mean})
        cl_data_m = cl_data - cx_mean

        cov_mat = np.cov(cl_data_m[:, 1:], rowvar=False)
        cov_lambda, cov_v = np.linalg.eig(cov_mat)
        e_order = np.argsort(-cov_lambda)
        cov_lambda = cov_lambda[e_order]
        cov_v = cov_v[:, e_order]

        # axis point to the positive x half-space
        # if cov_v[0][0] < 0 and cov_v[0][1] < 0 :
        #     cov_v *= -1.

        theta = np.degrees(np.arctan(cov_v[0, 1] / cov_v[0, 0]))

        eigs.update({cl: [theta, cov_lambda[0], cov_lambda[1], np.std(cl_data_m[:, 0])]})

        print("EigVals: ", cov_lambda)
        print(cov_v)
        print(theta)

        if make_plot:
            origin = [0.0, 0.0]
            ax = fig.add_subplot('22{}'.format(cl_i+1))
            ax.scatter(cl_data_m[:, 1], cl_data_m[:, 2], facecolor=p_colors[cl_i], edgecolor='', s=3)
            # ax.axvline(x=np.percentile(cl_data_m, q=98, axis=0)[0])
            # ax.axvline(x=np.percentile(cl_data_m, q=2, axis=0)[0], linestyle='--')

            ell = Ellipse(xy=(0, 0), width=2*np.sqrt(5.991*cov_lambda[0]), height=2*np.sqrt(5.991*cov_lambda[1]),
                          angle=theta)
            ell.set_facecolor('none')
            ax.add_artist(ell)

            ax2.scatter(cl_data[:, 1], cl_data[:, 2], facecolor=p_colors[cl_i], edgecolor='', s=3)
            ax.set_title('[{}] mean=({:.3f}, {:.3f})'.format(cl, cx_mean[1], cx_mean[2]))
            #ax.set_xlim(-0.8, 0.8)
            #ax.set_ylim(-0.8, 0.8)

    if make_plot:
        ax2.set_title('Class H+E+B')
        #ax2.set_xlim(-1.5, 1.5)
        #ax2.set_ylim(-1.5, 1.5)

        fig.suptitle("[{}] (cx, cy)-distribution for H, E, B classes".format(imname))

        plt.savefig(os.path.join(output_dir, 'CXCY_ClassDistribution_{}.png'.format(
            os.path.basename(imname)[:-4])), dpi=150)

    return means, eigs


def _bg_patch_positions(mask, n, lev, p_size, factor=1.0):
    """Generate n random points spread over mask area"""

    m_dil = cv2.erode(mask, (5, 5), iterations=16)
    m_dif = mask - m_dil
    idx = np.where(m_dif > 0)

    generated = []
    background = np.unique(mask)[0]
    while len(generated) < n:

        pt = np.random.randint(0, len(idx[0]))

        # pc_x = np.random.randint(0, int(factor*mask.shape[0]))
        # pc_y = np.random.randint(0, int(factor*mask.shape[1]))
        pc_x = idx[0][pt]
        pc_y = idx[1][pt]

        if m_dif[pc_x][pc_y] > background:
            if np.min(m_dif[pc_x:pc_x+p_size[0]//(2 ** lev), pc_y]) > background and \
                    np.min(m_dif[pc_x, pc_y:pc_y + p_size[0] // (2 ** lev)]) > background:
                generated.append((pc_y * (2 ** lev), pc_x * (2 ** lev)))
        else:
            continue

    return generated


def _patch_positions(mask, n, lev, factor=1.0):
    """Generate n random points spread over mask area"""

    generated = []
    background = np.unique(mask)[0]
    while len(generated) < n:

        pc_x = np.random.randint(0, int(factor*mask.shape[0]))
        pc_y = np.random.randint(0, int(factor*mask.shape[1]))

        xy_span = 8

        if np.min(mask[pc_x:pc_x+xy_span, pc_y:pc_y+xy_span]) > background:
            generated.append((pc_y * (2 ** lev), pc_x * (2 ** lev)))
        else:
            continue

    return generated


def _plot_hsd(data, classes, fig, splot_id, spname, splot_start=1):

    for i in range(3):
        ax = fig.add_subplot(splot_id+str(splot_start+i))
        ax.hist(data[np.where(classes == i)][:, i], bins=50)
        ax.set_title("[{}] Class {}".format(spname, i))


def get_patches_from_openslide(p_candidates):

    patches = []
    for p_cand in p_candidates:
        w_name = "Patch [{}]".format(p_cand)

        # (1) Sample patch from original image, and transform it to HSD space
        patch_image = np.array(slide_image.read_region(p_cand, p_level, p_size))[:, :, :3]
        patches.append(patch_image)

    return patches


def collect_distribution_samples(p_candidates, distributions, i0_ch, verbose=False):
    patches = []

    for patch_image in p_candidates:

        patch_image_mf = cv2.medianBlur(patch_image, 3)
        patches.append(np.copy(patch_image_mf))
        hsd_image, d_r = snu.transform_to_hsd(patch_image_mf, i0_ch)

        mean_color = np.mean(patch_image_mf, axis=(0, 1))
        print("Mean patch color:", mean_color)

        # Skip background-dominated tiles
        if np.sum((hsd_image[:, :, 0] < 0.2).astype(np.uint8)) > 0.6 * p_size[0] * p_size[1]:
            bg_ratio = 100.0 * np.sum((hsd_image[:, :, 0] < 0.2).astype(np.float)) / (p_size[0] * p_size[1])
            print("[skipped] Background {} %".format(bg_ratio))
            if verbose:
                w_name = "Skipped patch (high bcg)"
                cv2.namedWindow(w_name, cv2.WINDOW_NORMAL)
                cv2.resizeWindow(w_name, 512, 512)
                cv2.imshow(w_name, patch_image_mf)
                cv2.waitKey(0)
                cv2.destroyAllWindows()
            continue

        elif np.sum((hsd_image[:, :, 0] < 0.2).astype(np.uint8)) < 0.05 * p_size[0] * p_size[1]:
            bg_ratio = 100.0 * np.sum((hsd_image[:, :, 0] < 0.2).astype(np.float)) / (p_size[0] * p_size[1])
            print("[skipped] Background {} %".format(bg_ratio))
            if verbose:
                w_name = "Skipped patch (low bcg)"
                cv2.namedWindow(w_name, cv2.WINDOW_NORMAL)
                cv2.resizeWindow(w_name, 512, 512)
                cv2.imshow(w_name, patch_image_mf)
                cv2.waitKey(0)
                cv2.destroyAllWindows()
            continue


        # (2) Collect pixel samples from this patch to the distribution pool
        nc_vis_image = np.copy(patch_image)
        nuclei_maps, nc_keypoins = blob_detection_watershed((hsd_image[:, :, 0] > 1.0).astype(np.uint8), 2 ** p_level,
                                                            nc_vis_image, area_low=50, area_high=700, skip_watershed=True)
        eos_map = np.ones_like(hsd_image[:, :, 0]) - (hsd_image[:, :, 0] < 0.3).astype(np.float) - (
                    nuclei_maps[0] > 0.0).astype(np.float)

        if len(d_r[eos_map > 0]) < 10:
            continue

        d_r_thr = np.percentile(d_r[eos_map > 0], q=5)
        eos_sample_map = np.zeros_like(d_r) + eos_map - (d_r < d_r_thr).astype(np.float)
        hide_map = np.zeros_like(d_r)

        for i in range(hsd_image.shape[0]):
            for j in range(hsd_image.shape[1]):
                if max(abs(hsd_image[i, j, 1]), abs(hsd_image[i, j, 2])) > 2.0:
                    hide_map[i, j] = -1

        #        hide_map[ np.where(np.max(abs(hsd_image[:, :, 1]), abs(hsd_image[:, :, 2])) > 2.0)] = -1

        nc_points = sample_points(nuclei_maps[0], 0.5, 100)
        if len(nc_points) < 20:
            continue

        bc_points = sample_points(-hsd_image[:, :, 0], -0.2, len(nc_points))
        eos_points = sample_points(eos_sample_map + hide_map, 0.8, len(nc_points))

        t_points = sample_points(np.ones_like(eos_sample_map) + hide_map, 0.5, 30 * len(nc_points))

        p_image_cp = np.copy(patch_image)
        p_image_clf = np.copy(patch_image)

        point_sets = [nc_points, eos_points, bc_points, t_points]
        dict_targets = ['H', 'E', 'B', 'T']
        colors = [(0, 0, 255), (255, 0, 255), (255, 0, 0), (0, 0, 0)]
        last_added_sizes = []

        for pset, t, c in zip(point_sets, dict_targets, colors):
            for p in pset:
                v = np.concatenate([hsd_image[int(p[0]), int(p[1])], [p[0], p[1]]])
                if np.isfinite(np.sum(v)):
                    distributions[t].append(v)
                    cv2.drawMarker(p_image_cp, (int(p[1]), int(p[0])), color=c, markerType=cv2.MARKER_CROSS,
                                   markerSize=5)

            last_added_sizes.append(len(pset))

        if verbose:
            # (3) Get distribution parameters
            nn_clf, nn_xp = _get_distribution_parameters(distributions)

            if nn_xp is not None:
                for p, xp in zip(distributions['T'][-last_added_sizes[3]:], nn_xp[-last_added_sizes[3]:]):
                    c = colors[xp]
                    p_image_clf[int(p[3]), int(p[4])] = c

            w_name = "Distribution params"
            cv2.namedWindow(w_name, cv2.WINDOW_NORMAL)
            cv2.resizeWindow(w_name, 512, 512)
            cv2.imshow(w_name, np.vstack([
                np.hstack([p_image_cp, p_image_clf]),
                np.hstack([cv2.cvtColor(nuclei_maps[0].astype(np.uint8), cv2.COLOR_GRAY2RGB),
                           #cv2.cvtColor(255 * (hsd_image[:, :, 0] < 0.15).astype(np.uint8), cv2.COLOR_GRAY2RGB)
                           nc_vis_image
                           ])
            ]
            )
                       )
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    # if verbose and len(distributions['H']) > 0:
    #     fig = plt.figure()
    #     ax = fig.add_subplot(111, projection='3d')
    #
    #     ax.scatter(np.array(distributions['H'])[:, 1],
    #                np.array(distributions['H'])[:, 2],
    #                np.array(distributions['H'])[:, 0],
    #                s=5, facecolors='b', edgecolors='b')
    #     ax.scatter(np.array(distributions['E'])[:, 1],
    #                np.array(distributions['E'])[:, 2],
    #                np.array(distributions['E'])[:, 0],
    #                s=5, facecolors='r', edgecolors='r')
    #     ax.scatter(np.array(distributions['B'])[:, 1],
    #                np.array(distributions['B'])[:, 2],
    #                np.array(distributions['B'])[:, 0],
    #                s=5, facecolors='g', edgecolors='g')
    #
    #     plt.xlim(-3, 1)
    #     plt.ylim(-1, 3)
    #     plt.show()
    #
    #     plt.waitforbuttonpress()


if __name__ == "__main__":

    parser = argparse.ArgumentParser("Stain standardization")
    parser.add_argument('-i', '--input', type=str, required=True, help='Input WSI image')
    parser.add_argument('-m', '--tissue_mask', type=str, default=None,
                        help='Tissue mask image for i')

    parser.add_argument('-ml', '--mask_level', type=int, default=6,
                        help='Zooming level the mask was created from')
    parser.add_argument('-od', '--output_dir', type=str, required=True)
    parser.add_argument('-r', '--report', type=str, default=None)

    opt_group = parser.add_argument_group("[Patches output] (optional arguments)")
    opt_group.add_argument("--min-coverage", type=float, help='Minimal tissue coverage in accepted patches',
                           default=0.95, required=False)
    opt_group.add_argument('-o', '--patch_out', type=str, required=False, default=None,
                           help='Target directory to store output patches')
    opt_group.add_argument('-a', '--tumor_annot', type=str, default=None,
                           help='Tumor annotation')
    opt_group.add_argument("--csv", type=str, help='CSV file for saving the complete patch metadata',
                           default=None, required=False)
    opt_group.add_argument("--dump", type=str, help='Path prefix for dumping the minimal set of parameters for'
                                                    're-aplying the normalization', default=None, required=False)
    opt_group.add_argument("-l", "--extract-level", type=int, help='Level for extracting patches',
                           default=6, required=False)
    opt_group.add_argument("-p", "--patch-size", type=int, help='Size of the patch (on the base level 0)',
                           default=512, required=False)
    opt_group.add_argument("-s", "--patch-stride", type=int, help='Offset to next patch (on the base level 0)',
                           default=-1, required=False)
    opt_group.add_argument("--fix_csv", type=bool, default=False)

    args = parser.parse_args()
    input_image = args.input
    mask_image = args.tissue_mask
    mask_level = args.mask_level
    report_file = args.report
    output_dir = args.output_dir

    patch_out_root = args.patch_out
    tumor_annotation = args.tumor_annot

    if args.fix_csv:
        si = CamelyonSlideImage(image_path=input_image,
                                tissue_mask_path=mask_image,
                                tumor_annotation_file=tumor_annotation)

        # (1) Compute patch coverage
        p_shift = args.patch_stride
        if p_shift < 0:
            p_shift = args.patch_size

        p = si.get_annotated_patches(extract_level=args.mask_level,
                                     min_coverage_extraction=args.min_coverage,
                                     min_tumor_coverage=0.8, p_size=args.patch_size, p_shift=p_shift)

        # (2) Save patches
        patch_metadata = []
        for patch in p:
            location = patch[0]
            label = patch[1]
            patch_metadata.append(
                OrderedDict(
                    [("loc_x", location[0]), ("loc_y", location[1]),
                     ("sz_x", args.patch_size),
                     ("sz_y", args.patch_size),
                     ("label", label),
                     ("orig_file", input_image), ("mask", mask_image)
                     ]
                )
            )
        if args.csv is not None:
            dictlist_to_csv(patch_metadata, args.csv)

        quit()

    report_only = False
    if report_file is not None:
        report_only = True

    n_points = 50
    p_size = (512, 512)
    p_level = 0
    verbose = False

    # Read image metadata, create mask
    slide_image = OpenSlide(input_image)
    if mask_image is None:
        thumbnail = slide_image.read_region((0, 0), mask_level,
                                            size=slide_image.level_dimensions[mask_level])
        t_mask = cv2.threshold(
            cv2.cvtColor(thumbnail, cv2.COLOR_RGB2GRAY),
            128,
            cv2.THRESH_BINARY_INV
        )

    else:
        t_mask = cv2.imread(mask_image, cv2.IMREAD_GRAYSCALE)

    distributions = {'H': [],
                     'E': [],
                     'B': [],
                     'T': []}

    # Get channel intensity when no stain present
    p0_candidates = _patch_positions(((255 - t_mask) > 150).astype(np.uint8), 10, mask_level, factor=0.25)
    avg_background_patch = None
    skipped = 0
    for p0_cand in p0_candidates:
        p0_patch = np.array(slide_image.read_region(p0_cand, p_level, p_size), dtype=np.uint8)[:, :, :3]

        patch_mean = np.mean(p0_patch, axis=(0, 1))
        print("[Patch0] mean = ", patch_mean, end="")

        if not np.all(np.less(np.array([220.0, 220.0, 220.0]), patch_mean)):
            print(" [skipped] ")
            skipped += 1
            continue
        else:
            print("")

        if avg_background_patch is None:
            avg_background_patch = p0_patch.astype(np.uint32)
        else:
            avg_background_patch += p0_patch

    if len(p0_candidates)-skipped > 0:
        avg_background_patch = 1./(len(p0_candidates)-skipped) * avg_background_patch
        i0_ch = np.median(avg_background_patch, axis=(0, 1))
    else:
        i0_ch = [255.0, 255.0, 255.0]

    if patch_out_root is not None:

        if not os.path.isdir(patch_out_root):
            os.mkdir(patch_out_root)

    # FIXME The mask threshold (here = 100) may vary between different datasets
    p_candidates = _patch_positions((t_mask > 100).astype(np.uint8), n_points, mask_level)

    patches = get_patches_from_openslide(p_candidates)
    collect_distribution_samples(patches, distributions, i0_ch, verbose)

    nn_clf, nn_xp = _get_distribution_parameters(distributions)
    cluster_means, cluster_eigs = _get_clusters(distributions, nn_xp, os.path.basename(input_image),
                                                output_dir, make_plot=False)

    if report_only:

        if not os.path.exists(report_file):
            rfh_first = open(report_file, 'w+')

            rfh_first.write("image;H_mean_cx;H_mean_cy;H_eig_theta;H_eig_1;H_eig_2;"
                            "E_mean_cx;E_mean_cy;E_eig_theta;E_eig_1;E_eig_2;"
                            "B_mean_cx;B_mean_cy\n")
            rfh_first.close()

        with open(report_file, 'a') as rfh:

            out_str = "{};".format(os.path.basename(input_image))

            for cls in ['H','E']:
                out_str += "{};{};".format(
                    ';'.join(map(str, cluster_means[cls][1:])),
                    ';'.join(map(str, cluster_eigs[cls][:3]))
                )

            out_str += ';'.join(map(str, cluster_means['B'][1:]))

            rfh.write(out_str+"\n")

    else:

        template_means = {
            'H': np.array([-0.31, 0.294]),
            'E': np.array([-0.27, 0.5]),
            'B': np.array([-1.163, 1.638])
        }

        template_eigs = {
            'H': np.array([0.0, 0.00301, 0.00192]),
            'E': np.array([0.0, 0.017, 0.0038]),
            'B': np.array([86.0, 15.4482, 4.5485])
        }

        template_density = {
            'H': np.array([1.35, 0.23]),
            'E': np.array([0.6, 0.225]),
            'B': np.array([0.12, 0.078])
        }

        template_params = {
            'means': template_means,
            'eigs': template_eigs,
            'density': template_density
        }

        transfer_params = snu.get_normalization_params(cluster_means, cluster_eigs, template_params)

        if args.dump is not None:

            d_path = args.dump + str(os.path.basename(input_image)[:-4]) + "_stains_params.pkl"
            with open(d_path, 'wb') as dofs:
                d_dict = {
                    'cluster_means': cluster_means,
                    'cluster_eigs': cluster_eigs,
                    'i0_ch': i0_ch,
                    'distributions': distributions
                }

                pickle.dump(d_dict, dofs)

        subdir_name = os.path.basename(input_image)[:-4]
        subdir_path = os.path.join(output_dir, subdir_name)
        if not os.path.isdir(subdir_path):
            os.mkdir(subdir_path)

        if patch_out_root is not None:

            if '.ndpi' in input_image:
                si = NDPISlideImage(image_path=input_image, tissue_mask_path=mask_image,
                                    tumor_annotation_file=tumor_annotation)

            else:
                si = CamelyonSlideImage(image_path=input_image,
                                        tissue_mask_path=mask_image,
                                        tumor_annotation_file=tumor_annotation)

            # (1) Compute patch coverage
            p_shift = args.patch_stride
            if p_shift < 0:
                p_shift = args.patch_size

            p = si.get_annotated_patches(extract_level=args.mask_level,
                                         min_coverage_extraction=args.min_coverage,
                                         min_tumor_coverage=0.8, p_size=args.patch_size, p_shift=p_shift)

            # (2) Save patches
            patch_metadata = []

            for patch in p:
                location = patch[0]
                label = patch[1]
                patch_metadata.append(
                    OrderedDict(
                        [("loc_x", location[0]), ("loc_y", location[1]),
                         ("sz_x", args.patch_size),
                         ("sz_y", args.patch_size),
                         ("label", label),
                         ("orig_file", input_image), ("mask", mask_image)
                         ]
                    )
                )
            if args.csv is not None:
                dictlist_to_csv(patch_metadata, args.csv)

            p_img = si.get_patch_visualization(args.mask_level, p, args.patch_size, show=False)
            cv2.imwrite(patch_out_root+'/{}_preview.png'.format(
                os.path.basename(input_image)[:-4]
            ), p_img)

            # (3) Process patches and store results
            p_i = 0
            l_time = 0
            i_time = 0
            for patch_d in patch_metadata:
                location = (patch_d['loc_x'], patch_d['loc_y'])
                p_size = (patch_d['sz_x'], patch_d['sz_y'])
                label = patch_d['label']

                orig_patch = si.load_patch(location, p_size, args.extract_level)

                # mean filtering to reduce mosaicking present in the data
                ori_patch = cv2.cvtColor(orig_patch, cv2.COLOR_RGBA2RGB)
                ori_patch_mf = cv2.medianBlur(ori_patch, 3)

                patch_normalized = snu.normalize_stains_in_hsd(ori_patch_mf, i0_ch, t_shift, t_scale, t_rot, t_density, distributions)

                patch_m_values = np.mean(patch_normalized, axis=(0, 1))
                if np.min(patch_m_values) > 0.9 * np.max(patch_m_values) > 200.0:
                    print("Skipped patch: ", location)
                    continue

                patch_out_file = "{}_px{:d}_py{:d}_s{:d}_l{:d}.tif".format(
                    os.path.basename(input_image)[:-4],
                    location[0], location[1],
                    p_size[0],
                    args.extract_level
                )

                cv2.imwrite(patch_out_root+'/'+patch_out_file, patch_normalized)

