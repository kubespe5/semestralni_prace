# Hluboké učení pro zpracování histopatologických dat

Tento repozitář obsahuje řadu skriptů a programů pro natrénování CNN na datasetu petacc. Trénování a evaluace probíhá ve třech krocích.

 1. Příprava trénovacího datasetu
	 *  Trénovací dataset je vytvářen spuštěním skriptu `extract_multiple.py`, který načte anotované snímky, rozdělí je na patche, patche rozstřídí na testovací a trénovací data a uloží. Název patche končí buď na TU, repektive NO, pokud obsahuje či neobsahuje nádorovou tkáň. Cesty ke zdrojovým datům je nutné nastavit ve třídě `helper.py` a případné úpravy extrakce patchů pro jednotlivé obrázky je možné ve skriptu `patch_extraction.py`. 
2. Natrénování modelu
	* Natrénování a uložení modelu probíhá ve skriptu `train_model.py` a augmentace a načtení datasetu v souboru `tumors_dataset.py`.
3. Evaluace dat
	* Kvalitu modelu lze posoudit dvěma způsoby. Prvním je vypočítání přesnosti modelu na vybraném testovacím datasetu pomocí skriptu `evaluate_model.py`. Druhým způsobem je vizuální klasifikace snímku pomocí skriptu `image_classifier.py`. Pokud bychom chtěli oklasifikovat vícero snímků najednou, použijeme skript `create_report.py`.

Obecně byly skripty používány hodně specificky a často je k vyžadovanému úkolu měnit zdrojový kód, protože skripty nepřijímají žádné parametry.

