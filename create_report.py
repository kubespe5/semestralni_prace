from evaluate_model import evaluate_model
from image_classifier import classify_image
from helper import DEVICE, get_image_paths
import torch

models = [
    # "model30",
    # "model31"
    # "model32",
    # "model33",
    # "model34",
    # "model35",
    # "model36",
    # "model37",
    # "model38",
    # "model39",
    # "model310",
    # "model311",
    # "model312",
    # "model313",
    "model4146"
]

for model_name in models:
    print("Evaluating", model_name)
    model = torch.load(model_name, map_location=torch.device(DEVICE))
    #evaluate_model(model)

    print("Classifing using", model_name)

    images = get_image_paths("/tmp/kubespe5/test_source/images")

    for i,image in enumerate(images):
        if image.startswith(".") or image.endswith(".png"):
            continue
        mask = (image[:-5] + "_mask_l6.png").replace("images", "masks")
        classify_image(model, image, mask, str(i) + ".jpg")
        