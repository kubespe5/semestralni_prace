import cv2
import numpy as np


def separation(im, patch_list, n_augm=0, is_camelyon=False, swap=True,
               rgb_from_her=None, her_from_rgb=None):
    if rgb_from_her is None:
        rgb_from_her = np.array([[0.4605, 0.7538, 0.3914],
                                 [0.2948, 0.7491, 0.5873],
                                 [0.2720, 0.8782, 0.3852]])

        if is_camelyon:
            rgb_from_her = np.array([[0.5298, 0.8073, 0.2544],
                                     [0.4258, 0.7706, 0.4715],
                                     [0.2188, 0.8858, 0.3651]])

    if her_from_rgb is None:
        her_from_rgb = np.linalg.inv(rgb_from_her)

    rgb = 1 / 255.0 * (np.copy(im).astype(np.float32) + 1)
    rgb = rgb.reshape((-1, 3))
    if swap:
        rgb[:, [0, 2]] = rgb[:, [2, 0]]

    stains = np.dot(-np.log(rgb), her_from_rgb)

    r_stains = np.zeros_like(stains)
    h_stains = np.zeros_like(stains)
    e_stains = np.zeros_like(stains)

    im_sep = stains

    for i in range(n_augm):

        augm_alpha = 1.0 + 0.01 * np.random.randint(-10, 10, 3)
        augm_beta = 0.01 * np.random.randint(-10, 10, 3)

        augm_stains = augm_alpha * stains + augm_beta
        im_A = 255 * np.exp(np.dot(-augm_stains, rgb_from_her))
        if swap:
            im_A[:, [0, 2]] = im_A[:, [2, 0]]
        imA = np.reshape(np.clip(im_A, 0, 255), im.shape).astype(np.uint8)

        if n_augm == 1:
            im_sep = augm_stains

        patch_list.append(imA.astype(np.uint8))

    r_stains[:, 0] = np.copy(im_sep[:, 0])
    im_R = 255 * np.exp(np.dot(-r_stains, rgb_from_her))
    imR = cv2.cvtColor(np.reshape(np.clip(im_R, 0, 255), im.shape).astype(np.uint8),
                       cv2.COLOR_RGB2GRAY)

    h_stains[:, 1] = np.copy(im_sep[:, 1])
    im_H = 255 * (1 - np.exp(np.dot(-h_stains, rgb_from_her)))
    imH = np.reshape(np.clip(im_H, 0, 255), im.shape).astype(np.uint8)[:, :, 1]

    e_stains[:, 2] = np.copy(im_sep[:, 2])
    im_E = 255 * (1 - np.exp(np.dot(-e_stains, rgb_from_her)))
    imE = np.reshape(np.clip(im_E, 0, 255), im.shape).astype(np.uint8)[:, :, 1]

    return imH, imE, imR