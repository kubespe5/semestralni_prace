import os

PATH = "model314"
# TMP_TRAIN_PATH= "data/tmp"
TMP_TRAIN_PATH= "/tmp/kubespe5/test2048"

IMAGES_PATH = "/local/temporary/kubespe5/test_source/images"
MASKS_PATH = "/local/temporary/kubespe5/test_source/masks"
ANOTATIONS_PATH = "/local/temporary/kubespe5/test_source/annotations"
# IMAGES_PATH = "data/petacc3/images"
# MASKS_PATH = "data/petacc3/tissue_masks"
# ANOTATIONS_PATH = "data/petacc3/annotations"

DEVICE = "cuda:1"

def get_image_paths(data_path):
    image_paths = []
    for r, d, f in os.walk(data_path):
        for file in f:
            if file.startswith("._"):
                continue
            image_paths.append(os.path.join(r, file))

    return image_paths