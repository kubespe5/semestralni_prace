import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader

import tumors_dataset
from helper import get_image_paths, PATH, TMP_TRAIN_PATH, DEVICE


def evaluate_model(model=None):
    if model is None:
        model = torch.load(PATH, map_location=torch.device(DEVICE))

    model.eval()

    testDataset = tumors_dataset.TumorsDataset(get_image_paths("/tmp/kubespe5/test2048/data"))
    data_loader = DataLoader(testDataset, batch_size=50, shuffle=False, num_workers=2)

    device = torch.device(DEVICE if torch.cuda.is_available() else "cpu")

    correct = 0
    total = 0

    tp = 0
    tn = 0
    fp = 0
    fn = 0

    for batch in data_loader:
        predictions = torch.round(model(batch['image'].to(device))).cpu()

        truth = batch['is_tumor'] == 1

        tp += ((truth == predictions) & truth).sum().item()
        tn += ((truth == predictions) & ~truth).sum().item()
        fp += ((truth != predictions) & truth).sum().item()
        fn += ((truth != predictions) & ~truth).sum().item()

        correct += (truth == predictions).sum().item()
        total += truth.size(0)


    acc = correct / total
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)

    print("total: %d" % total)
    print("correct: %d" % correct)

    print("tp: %.3f" % tp)
    print("tn: %.3f" % tn)
    print("fp: %.3f" % fp)
    print("fn: %.3f" % fn)


    print("accuracy: %.3f" % acc)
    print("precision: %.3f" % precision)
    print("recall: %.3f" % recall)

    return acc


if __name__ == '__main__':
    evaluate_model()
