import torch
import torchvision.models as models
from torch import nn
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader

from evaluate_model import evaluate_model
from helper import get_image_paths, PATH, TMP_TRAIN_PATH
from tumors_dataset import TumorsDataset


def main():
    trainDataset = TumorsDataset(get_image_paths(TMP_TRAIN_PATH + "/train"))
    batch_size = 100
    data_loader = DataLoader(trainDataset, batch_size=batch_size, shuffle=True, num_workers=2)

    device = torch.device("cuda:1" if torch.cuda.is_available() else "cpu")
    inception = models.inception_v3(pretrained=True)

    # Replace the last layer
    inception.fc = nn.Sequential(
        nn.Linear(2048, 1),
        nn.Sigmoid()
    )

    inception.to(device)

    criterion = nn.BCELoss().to(device)
    optimizer = torch.optim.SGD(inception.parameters(), 0.1, momentum=0.8)
    scheduler = ReduceLROnPlateau(optimizer, mode='max', factor=0.3, patience=0, verbose=True)

    print("starting training")

    for epoch in range(15):
        inception.train()
        running_loss = 0.0

        for i, batch in enumerate(data_loader):
            print(".")

            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = batch['image'].to(device), batch['is_tumor'].to(device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs, _ = inception(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            n = 5

            if i % n == (n - 1):  # print every n mini-batches
                print('\n[%d, %5d] loss: %.3f' % (epoch + 1, i + 1, running_loss / n))
                running_loss = 0.0

        acc = evaluate_model(inception)
        scheduler.step(acc)
        inception.train()
        torch.save(inception, PATH + str(epoch))

    print("training finished")

if __name__ == '__main__':
    main()
    evaluate_model()
