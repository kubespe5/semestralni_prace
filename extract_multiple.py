from helper import get_image_paths, IMAGES_PATH, MASKS_PATH, ANOTATIONS_PATH, TMP_TRAIN_PATH
from patch_extraction import extract_patches_from_image
import os
import shutil

if __name__ == "__main__":
    split = False

    image_paths = get_image_paths(IMAGES_PATH)
    mask_paths = get_image_paths(MASKS_PATH)
    annotation_paths = get_image_paths(ANOTATIONS_PATH)

    tmp_folder =  TMP_TRAIN_PATH

    
    # Remove and create tmp directory, so that it does not get mixed up with older data
    if os.path.exists(tmp_folder):
        shutil.rmtree(tmp_folder)
    os.mkdir(tmp_folder)
    if split:
        os.mkdir(tmp_folder + '/train')
        os.mkdir(tmp_folder + '/test')
        os.mkdir(tmp_folder + '/validate')
    else:
        os.mkdir(tmp_folder + '/data') 



    i = 0
    print(image_paths)

    for image_path in image_paths:
        # Skip hidden macos files and other trash
        if "/." in image_path or "_mask_" in image_path:
            continue

        print(image_path)    
        image_name = os.path.basename(image_path)
        mask_name = image_name[:-5] + "_mask_l6.png"
        mask_path = MASKS_PATH + "/" + mask_name

        has_annotation = False
        for annotation_path in annotation_paths:
            if image_name in annotation_path:
                has_annotation = True
                break

        if has_annotation and mask_path in mask_paths:
            # Use some images for validation
            if i % 15 == 1 and split:
                shutil.copy(image_path, tmp_folder + "/validate/" + image_name)
            else:
                extract_patches_from_image(image_path, mask_path, tmp_folder)
            i += 1
        else:
            print("Mask or annotation not found")