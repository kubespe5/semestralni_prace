import torch
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms


class TumorsDataset(Dataset):

    def __init__(self, image_paths):
        self.image_paths = image_paths

        self.transform = transforms.Compose([
            transforms.Resize(299),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])]
        )

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, idx):
        image = Image.open(self.image_paths[idx])
        is_tumor = [1] if 'TU.jpg' in self.image_paths[idx] else [0]

        return {
            'image': self.transform(image),
            'is_tumor': torch.Tensor(is_tumor)
        }
