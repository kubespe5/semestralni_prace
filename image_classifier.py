import cv2
import torch
from torchvision.transforms import transforms

from helper import PATH, DEVICE
from microscopyio.slide_image import NDPISlideImage

IMAGE_PATH = "/tmp/kubespe5/petacc3/validate/used.ndpi"
MASK_PATH = "/tmp/kubespe5/petacc3/validate/used-mask.png"

    
def classify_image(model=None, image_path=None, image_mask_path=None, name="test.jpg"):
    if model is None:
        model = torch.load(PATH, map_location=torch.device(DEVICE))

    if image_path is None or image_mask_path is None:
        image_path = IMAGE_PATH
        image_mask_path = MASK_PATH

    slide_image = NDPISlideImage(image_path=image_path, tissue_mask_path=image_mask_path)

    patch_size = 512

    patches = slide_image._compute_patch_cover(
        probe_level=6,
        patch_size=patch_size,
        patch_shift=patch_size
    )

    model.eval()

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                            std=[0.229, 0.224, 0.225])]
    )

    def getPredictionForPatch(patch_pos):
        patch = slide_image.load_patch(patch_pos, (patch_size, patch_size), 1)

        nn_ready_img = transform(patch)[None, ...].to(DEVICE)
        res = torch.round(model(nn_ready_img)).item()
    
        return res

    labeled_patches = list(map(lambda patch: [patch, getPredictionForPatch(patch)], patches))

    p_img = slide_image.get_patch_visualization(6, labeled_patches, patch_size , show=False)
    cv2.imwrite(name, p_img)

if __name__ == '__main__':
    classify_image()
