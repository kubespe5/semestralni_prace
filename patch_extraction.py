"""
    Extract patch meta-data from a microscopy image (given a tissue mask file and an annotation file, when available)

    Input is a microscopy image (currently tiff or ndpi) and a tissue mask file.

    Output is a CSV file containing the coordinates of the upper left corner for each valid
    patch (tissue coverage condition, etc.) A tissue mask must be provided, optionally also a tumor annotation
    file can be specified (in such case, the labels of each patch will be set accordingly).
    If the --preview option is specified, also a png file with visualized patch boundaries is written to the specified
    path.

    Author:
        Jan Hering (BIA/CMP)
        jan.hering@fel.cvut.cz
"""

import os
import cv2

from microscopyio.slide_image import NDPISlideImage, CamelyonSlideImage


def extract_patches_from_image(image_path, mask_path, tmp_folder, patch_size=1024, resized_patch_size=299, min_coverage=0.95, split=False):
    target_level = 0

    si = NDPISlideImage(image_path=image_path, tissue_mask_path=mask_path)
    patches = si.get_annotated_patches(extract_level=6,
                                       min_coverage_extraction=min_coverage,
                                       min_tumor_coverage=0.01, p_size=patch_size, p_shift=patch_size)

    # Count the number of tumorous patches
    n_tu, n_no, n_bo = 0, 0, 0
    p_sel_i = 0
    while p_sel_i < len(patches):
        p_label = patches[p_sel_i][1]
        if p_label == 'TU':
            n_tu += 1
        elif p_label == 'BO':
            n_bo += 1
        else:
            n_no += 1
        p_sel_i += 1

    p_img = si.get_patch_visualization(6, patches, patch_size, show=False)
    cv2.imwrite(tmp_folder + "/visualization_{}.jpg".format(os.path.basename(image_path[:-4])), p_img)

    for pid, patch in enumerate(patches):
        # Load the full image
        patch0 = si.load_patch(patch[0], (patch_size, patch_size), level=target_level)

        # Resize the image to the input shape of nn
        patch0 = cv2.resize(patch0, (resized_patch_size, resized_patch_size))

        # Save every nth into testing folder
        if split:
            if pid % 20 == 0:
                folder = "test"
            else:
                folder = "train"
        else:
            folder = "data"

        out_imname = tmp_folder + '/' + folder + '/{}_tile_{:02d}_{}.jpg'.format(os.path.basename(image_path[:-4]), pid, patch[1])
        print(out_imname)

        cv2.imwrite(out_imname, patch0)
